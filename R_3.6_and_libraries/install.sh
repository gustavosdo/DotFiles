# Download and install R 3.6 and RStudio, Plangea package and its dependencies

printf "\n%s\n" "A script to install R 3.6 and its libraries in order to run the plangea software in Ubuntu 18.04 and Linux Mint 19"
printf "\n%s\n" "Administrator privileges required!"

printf "\n%s\n" "Updating the current system..."
sudo apt update && sudo apt upgrade

printf "\n%s\n" "Installing oracle Java v10"
sudo apt-get purge openjdk*
sudo add-apt-repository ppa:linuxuprising/java
sudo apt-get update
sudo apt-get install oracle-java12-installer
sudo apt install oracle-java12-set-default

printf "\n%s\n" "Adding cran-35 repo."
echo "deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/" | sudo tee -a /etc/apt/sources.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9

printf "\n%s\n" "Updating repos. list and installing R components"
sudo apt update && sudo apt install r-base* r-cran-boot r-cran-class r-cran-cluster r-cran-codetools r-cran-foreign r-cran-kernsmooth r-cran-lattice r-cran-mass r-cran-matrix r-cran-mgcv r-cran-nlme r-cran-nnet r-cran-rpart r-cran-spatial r-cran-survival r-cran-rodbc coinor-symphony debhelper r-base-dev cdbs coinor-libsymphony* coinor-libcgl-dev autotools-dev libgdal* libproj-dev pigz libcurl4-openssl-dev libssl-dev
mkdir ~/Rlibs
echo "R_LIBS=~/Rlibs" > .Renvrion
Rscript packagesPlangea.R

# Download data
# create folders and move data
# configure json config file (the input/output folders)

printf "\n%s\n" "Downloading plangea"
Rscript run_plangea.R

wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.2.1335-amd64.deb
sudo apt ./install rstudio-1.2.1335-amd64.deb
